import 'react-perfect-scrollbar/dist/css/styles.css';
import React from 'react';
import { useRoutes } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'src/components/GlobalStyles';
import 'src/mixins/chartjs';
import theme from 'src/theme';
import routes from 'src/routes';

class App extends React.Component {

  render() {
    return (
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        {this.props.routing}
      </ThemeProvider>
    );
  }
};

const WithRoutes = () => {
  const routing = useRoutes(routes);

  return (<App routing={routing}/>);
}

export default WithRoutes;
