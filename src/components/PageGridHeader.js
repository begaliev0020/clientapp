import React from 'react';
import { useNavigate } from 'react-router-dom';
import {
    Box,
    Button,
} from '@material-ui/core';
import 'react-sliding-side-panel/lib/index.css';

class PageGridHeader extends React.Component {
    render() {
        return (
            <div>
                <h1><span>{this.props.title}</span></h1>
                <Box display="flex" justifyContent="flex-start" mt={3}>
                    <Box mr={1}>
                <Button color="primary" variant="contained" onClick={() => this.props.navigate('/app/' + this.props.controllerName + '/addedit?id=0', { replace: true })}>
                            Добавить
                        </Button>
                    </Box>
                </Box>
            </div>
        );
    }
}

function WithNavigate(props) {
  let navigate = useNavigate();
  return <PageGridHeader {...props} navigate={navigate} />
}

export default WithNavigate
