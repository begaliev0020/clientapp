import * as React from 'react';
import 'react-sliding-side-panel/lib/index.css';
import PageGridHeader from 'src/components/PageGridHeader';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import BuildIcon from '@material-ui/icons/Build';
import { useNavigate } from 'react-router-dom';
import MUIDataTable from "mui-datatables";
import {
  Box,
  Button,
} from '@material-ui/core';



class PageGrid extends React.Component {
  constructor(props) {
    super(props);

    var cols = this.props.columns;
    var editcolumn = {
      name: "ДЕЙСТВИЯ",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <React.Fragment>
              <IconButton onClick={() => {
                var id = this.props.data[dataIndex].id;
                this.props.navigate('/app/' + this.props.controllerName + '/addedit?id='+ id, { replace: true })
              }}>
                <BuildIcon />
            </IconButton>
              <IconButton onClick={() => {
                var id = this.props.data[dataIndex].id;
                if (window.confirm('Вы уверены?')) {
                  fetch('/' + this.props.controllerName + '/Delete?id=' + id)
                    .then(res => this.props.onItemDeleted());
                  
                } else {
                  return false;
                }

              }}>
                <DeleteIcon />
            </IconButton>
            </React.Fragment >
          );
        }
      }
    };
    cols.splice(0, 0, editcolumn);

    this.state = {
      currentId: 0,
      columns: cols
    };
  }

  handleRowDeletes(t, rowsDeleted, data) {
    if (window.confirm('Вы уверены?')) {
      for (var i = 0; i < rowsDeleted.data.length; i++) {
        var item = t.props.data[rowsDeleted.data[i].dataIndex];
        fetch('/' + this.props.controllerName + '/Delete?id=' + item.id);
      }
      return true;
    } else {
      return false;
    }
  }

  render() {

    const options = {
      filter: true,
      filterType: 'dropdown',
      responsive: 'vertical',
      onColumnSortChange: (changedColumn, direction) => console.log('changedColumn: ', changedColumn, 'direction: ', direction),
      onChangeRowsPerPage: numberOfRows => console.log('numberOfRows: ', numberOfRows),
      onChangePage: currentPage => console.log('currentPage: ', currentPage),
      onRowsDelete: (rowsDeleted, data) => this.handleRowDeletes(this, rowsDeleted, data)
    };

    return (
      <React.Fragment>
        <PageGridHeader controllerName={this.props.controllerName} title={this.props.title}>
        </PageGridHeader>
        <Box mt={3}>
          <MUIDataTable title={this.props.subtitle} data={this.props.data} columns={this.state.columns} options={options} />
        </Box>
      </React.Fragment>
    );
  }
}

function WithNavigate(props) {
  let navigate = useNavigate();
  return <PageGrid {...props} navigate={navigate} />
}

export default WithNavigate
