import React from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  Checkbox,
  Select,
  MenuItem,
  InputBase,
  NativeSelect,
  FormControl,
  InputLabel
} from '@material-ui/core';
import 'react-sliding-side-panel/lib/index.css';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import { Children } from 'react';

export default class BaseLookup extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      openPanel: false,
      currentId: 0
    };

  }

  render() {
    return (
      <React.Fragment>
        <Grid container direction="row" spacing={0}>
          <Grid item xs={11}>
            <NativeSelect id={this.props.id} label={this.props.label} value={ this.props.value}
              name={this.props.name} variant="outlined" fullWidth onChange={(e) => {
                this.props.onChange(e);
              }}
            >
              <option key={this.props.name + '' + 0} aria-label="None" value="0" />
              {
                this.props.data == null? "" : this.props.data.map((book) => (
                <option key={this.props.name + '' + book.id} value={book.id}>{book.name}</option>
              ))}
            </NativeSelect>
          </Grid>
          <Grid item xs={1}>
            <IconButton onClick={this.props.onButtonAddClick}><AddIcon /></IconButton>
          </Grid>
          {/*<Grid item xs={2}>
            <Button onClick={this.props.onButtonEditClick}>Edit</Button>
          </Grid>*/}
        </Grid>
        {this.props.children}
      </React.Fragment>
    );
  }
}

