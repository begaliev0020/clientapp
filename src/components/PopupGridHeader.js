import React from 'react';
import {
    Box,
    Button,
} from '@material-ui/core';
import 'react-sliding-side-panel/lib/index.css';

export default class PopupGridHeader extends React.Component {
    render() {
        return (
            <div>
                <h1><span>{this.props.title}</span></h1>
                <Box display="flex" justifyContent="flex-start" mt={3}>
                    <Box mr={1}>
                <Button color="primary" variant="contained" onClick={this.props.onButtonAddClick}>
                            Добавить
                        </Button>
                    </Box>
                </Box>
            </div>
        );
    }
}
