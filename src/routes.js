import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import MainLayout from 'src/layouts/MainLayout';
import AccountView from 'src/views/account/AccountView';
import CountryListView from 'src/views/country/CountryListView';
import CountryAddEditView from 'src/views/country/CountryAddEditView';
import PersonListView from 'src/views/person/PersonListView';
import PersonAddEditView from 'src/views/person/PersonAddEditView';
import DashboardView from 'src/views/reports/DashboardView';
import LoginView from 'src/views/auth/LoginView';
import NotFoundView from 'src/views/errors/NotFoundView';
import ProductListView from 'src/views/product/ProductListView';
import RegisterView from 'src/views/auth/RegisterView';
import SettingsView from 'src/views/settings/SettingsView';
import CitizenshipAddEditView from 'src/views/citizenship/CitizenshipAddEditView';
import CitizenshipListView from 'src/views/citizenship/CitizenshipListView';
import TaskListView from 'src/views/task/TaskListView';
import TaskAddEditView from 'src/views/task/TaskAddEditView';
import ProfileView from './views/profile/ProfileView';

const routes = [
  {
    path: 'app',
    element: <DashboardLayout />,
    children: [
      { path: 'account', element: <ProfileView /> },
      { path: 'country', element: <CountryListView /> },
      { path: 'country/addedit', element: <CountryAddEditView /> },
      { path: 'task', element: <TaskListView /> },
      { path: 'task/addedit', element: <TaskAddEditView /> },
      { path: 'person', element: <PersonListView /> },
      { path: 'person/addedit', element: <PersonAddEditView /> },
      { path: 'dashboard', element: <DashboardView /> },
      { path: 'products', element: <ProductListView /> },
      { path: 'citizenshipList', element: <CitizenshipListView /> },
      { path: 'citizenship', element: <CitizenshipAddEditView /> },
      { path: 'settings', element: <SettingsView /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <LoginView /> },
      { path: 'register', element: <RegisterView /> },
      { path: '404', element: <NotFoundView /> },
      { path: '/', element: <Navigate to="/app/dashboard" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
