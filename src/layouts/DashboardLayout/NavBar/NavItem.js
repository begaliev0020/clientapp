import React from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Button,
  ListItem, List, ListItemIcon, ListItemText,
  makeStyles, Collapse
} from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import {
  ChevronDown, ChevronUp
} from 'react-feather';
//import Collapse from '@material-ui/core/Collapse';

const useStyles = makeStyles((theme) => ({
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0
  },
  button: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
    justifyContent: 'flex-start',
    letterSpacing: 0,
    padding: '10px 8px',
    textTransform: 'none',
    width: '100%'
  },
  icon: {
    marginRight: theme.spacing(1)
  },
  title: {
    marginRight: 'auto'
  },
  active: {
    color: theme.palette.primary.main,
    '& $title': {
      fontWeight: theme.typography.fontWeightMedium
    },
    '& $icon': {
      color: theme.palette.primary.main
    }
  }
}));

const NavItem = ({
  className,
  href,
  icon: Icon,
  title,
  nested,
  ...rest
}) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  var loginButton, loginButton2;
  if (nested != null) {
    loginButton = <Button
      className={classes.button}
      onClick={handleClick}
    >
      {Icon && (
        <Icon
          className={classes.icon}
          size="20"
        />
      )}
      <span className={classes.title}>
        {title}
      </span>
      {open ? <ChevronUp className={classes.icon}
        size="20" /> : <ChevronDown className={classes.icon}
          size="20" />}
    </Button>;
    loginButton2 = <Collapse in={open} timeout="auto" unmountOnExit>
      <List component="div" disablePadding>
        {nested.map((item) => (
          <ListItem className={clsx(classes.item)} key={ item.title }>
            <Button
              activeClassName={classes.active}
              className={classes.button}
              component={RouterLink}
              to={item.href}
            >
              {item.icon && (
                <Icon
                  className={classes.icon}
                  size="20"
                />
              )}
              <span className={classes.title}>
                {item.title}
              </span>
            </Button>

          </ListItem>
        ))}

      </List>
    </Collapse>;
  } else {
    loginButton = <Button
      activeClassName={classes.active}
      className={classes.button}
      component={RouterLink}
      to={href}
    >
      {Icon && (
        <Icon
          className={classes.icon}
          size="20"
        />
      )}
      <span className={classes.title}>
        {title}
      </span>
    </Button>;
  }

  return (
    <div>
      <ListItem
        className={clsx(classes.item, className)}
        disableGutters
        {...rest}
      >
        {loginButton}

      </ListItem>
      {loginButton2}
    </div>
  );
};

NavItem.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  icon: PropTypes.elementType,
  title: PropTypes.string
};

export default NavItem;
