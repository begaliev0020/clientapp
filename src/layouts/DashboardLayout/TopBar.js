import React, { useState } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  AppBar,
  Badge,
  Box,
  Hidden,
  IconButton,
  Toolbar,
  makeStyles
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import Logo from 'src/components/Logo';
import 'src/authToken';
import * as signalR from '@microsoft/signalr';

const localStorageKey = '__test_token__'

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    width: 60,
    height: 60
  }
}));

class TopBar extends React.Component {

  constructor(props) {
    super(props);

    this.connection = null;
    this.onTaskReceived = this.onTaskReceived.bind(this);

    this.state = {
      notifications: '',
      counter2: 0,
    };
  }

  onNotifReceived(res) {
    this.setState({ counter: this.state.counter + 1});
  }
  
  onTaskReceived(res) {
    this.setState({ counter2: res});
  }

  componentDidMount() {
    const protocol = new signalR.JsonHubProtocol();

    const transport = signalR.HttpTransportType.WebSockets;

    const options = {
      transport,
      logMessageContent: true,
      logger: signalR.LogLevel.Trace,
      accessTokenFactory: () => this.props.accessToken,
    };

    // create the connection instance
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl('/message', options)
      .withHubProtocol(protocol)
      .build();

    this.connection.on('TaskAdded', this.onTaskReceived);

    this.connection.start()
      .then(() => console.info('SignalR Connected'))
      //.then(() => this.connection.invoke('Send', 'some test text'))
      .catch(err => console.error('SignalR Connection Error: ', err));
  }

  componentWillUnmount() {
    this.connection.stop();
  }

  onNotificationClicked2() {
    alert('clicked');
  }

  render() {
    return (
      <AppBar
        className={clsx(this.props.classes.root, this.props.className)}
        elevation={0}
        
      >
        <Toolbar>
          <RouterLink to="/">
            <Logo />
          </RouterLink>
          <Box flexGrow={1} />
          {/*
          <Hidden xsDown>*/
          }
          <IconButton color="inherit">
            <Badge
              badgeContent={this.state.counter2}
              color="error"
            >
              <NotificationsIcon onClick={ () => this.onNotificationClicked2()} />
            </Badge>
          </IconButton>
          <IconButton color="inherit">
            <InputIcon onClick={() => {
              localStorage.removeItem(localStorageKey);
              this.props.navigate('/login', { replace: true });
            }} />
          </IconButton>
          {/*</Hidden>*/}
          <Hidden mdUp>
            <IconButton
              color="inherit"
              onClick={this.props.onMobileNavOpen}
            >
              <MenuIcon />
            </IconButton>
          </Hidden>
        </Toolbar>
      </AppBar>
    );
  }
};

TopBar.propTypes = {
  className: PropTypes.string,
  onMobileNavOpen: PropTypes.func
};


const WithRoutes = ({
  className,
  onMobileNavOpen,
  ...rest
}) => {

  const classes = useStyles();
  const navigate = useNavigate();

  return (<TopBar navigate={navigate} classes={classes} className={className} onMobileNavOpen={onMobileNavOpen} rest />);
}

export default WithRoutes;
