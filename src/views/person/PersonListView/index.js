import React, { useState } from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import PageGrid from 'src/components/PageGrid';

class PersonListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      openPanel: false,
      currentId: 0
    };
  }

  onItemDeleted() {
    this.loadPersons();
  }

  loadPersons() {
    var url = '/Person/GetAll';
    userApiClient(url)
      .then(json => this.setState({ data: json }));
  }

  componentDidMount() {
    this.loadPersons();
  }

  render() {
    const columns = [
      {
        name: "fullName",
        label: "ФИО",
        options: {
          filter: true,
        }
      },
      {
        label: "Дата рождения",
        name: 'strDateBirth',
        options: {
          filter: true,
        }
      },
      {
        label: "Есть авто",
        name: 'hasAuto',
        options: {
          filter: true,
        }
      },
      {
        label: "Рост",
        name: 'height',
        options: {
          filter: true,
        }
      },
      {
        label: "Вес",
        name: 'weight',
        options: {
          filter: true,
        }
      },
      {
        label: "Страна",
        name: 'idCountryBornNavName',
        options: {
          filter: true,
        }
      },
      {
        label: "Рабочий день",
        name: 'strWorkDayStart',
        options: {
          filter: true,
        }
      },
    ];
    return (
      <Page title="Persons">

        <Container maxWidth={false}>

          <PageGrid
            title="Люди 1"
            subtitle="Люди 2"
            onItemDeleted={() => this.onItemDeleted(this)}
            columns={columns}
            data={this.state.data}
            controllerName="person" />

        </Container>
      </Page>
    );
  }

};


const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

function WithAllProps(props) {
  let classes = useStyles();
  return <PersonListView {...props} classes={classes} />
}

export default WithAllProps
