import React from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import Page from 'src/components/Page';
import { useNavigate } from 'react-router-dom';
import { useParams, useLocation } from "react-router";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  Checkbox,
  FormControlLabel,
  Container,
  Select,
  MenuItem,
  InputBase,
  NativeSelect,
  FormControl,
  InputLabel
} from '@material-ui/core';
import BaseLookup from 'src/components/BaseLookup'

import SlidingPanel from 'react-sliding-side-panel';

//импорты всех панелек и гридов, которые используются
import CountryAddEditView from 'src/views/country/CountryAddEditView';
import AddressListView from 'src/views/address/AddressListView';
import CitizenshipListView from 'src/views/citizenship/CitizenshipListView';


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class PersonAddEditView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      fullName: '',
      dateBirth: '',
      hasAuto: false,
      height: '',
      weight: '',
      idCountryBorn: '',
      workDayStart: '',

      //для каждого справочника свой openFIELDPanel. Определение state-переменных
      openidCountryPanel: false,
      //openidSexPanel: false, пример

      //каждый справочник
      countries: []
      //sexes: [] пример
    };
  }

  async doLoad() {
    //загрузка справочников
    await this.loadCountries();

    //параметр, пришедший во всплывающее окно (с лукапа)
    var itemId = this.props.id;

    //если параметр пустой, то проверяем строку браузера - вдруг открываем не в окне (со списка)
    if (itemId == null || itemId == 0) {
      itemId = this.props.idParam / 1;
    }

    if (itemId == null || itemId == 0) {
      this.setState({
        id: 0,
        fullName: '',
        dateBirth: '',
        hasAuto: false,
        height: '',
        weight: '',
        idCountryBorn: '',
        workDayStart: ''
      });
      return;
    }

    var url = '/Person/GetOneById?id=' + itemId;
    userApiClient(url)
      .then(json => {
        this.setState({
          id: json.id,
          fullName: json.fullName,
          dateBirth: json.strDateBirth,
          hasAuto: json.hasAuto,
          height: json.height,
          weight: json.weight,
          idCountryBorn: json.idCountryBorn,
          workDayStart: json.strWorkDayStart
        });


      });
  }

  componentDidMount() {
    this.doLoad ();
  }

  loadCountries() {
    var url = '/Country/GetAll';
    return userApiClient(url)
      .then(json => this.setState({ countries: json }));
  }

  reloadCountries() {
    this.loadCountries();
    this.setState({ openidCountryPanel: false });
  }


  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  //для входящего справочника Add и Edit с лукапа
  btnAddCountryClick(t) {
    t.setState({ openidCountryPanel: true, currentId: 0 });
  }

  btnEditCountryClick(t, id) {
    t.setState({ openidCountryPanel: true, currentId: id });
  }


  render() {
    return (
      <Page title="Человек">
        <Container maxWidth="lg">
          <Grid container spacing={3}>

            <form autoComplete="off" noValidate>
              <Card>
                <CardHeader title="Человек" />
                <Divider />
                <CardContent>
                  <Grid container spacing={3}>

                    <Grid item md={12} xs={12}>
                      <TextField
                        fullWidth
                        label="ФИО"
                        name="fullName"
                        onChange={this.handleChange}
                        required
                        value={this.state.fullName}
                        variant="outlined"
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <TextField
                        fullWidth
                        type="date"
                        label="Дата рождения"
                        name="dateBirth"
                        onChange={this.handleChange}
                        value={this.state.dateBirth}
                        variant="outlined"
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <FormControlLabel
                        label="Есть авто"
                        control={
                          <Checkbox
                            fullWidth
                            name="hasAuto"
                            onChange={this.handleChange}
                            value={this.state.hasAuto}
                            variant="outlined"
                          />
                        }
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <TextField
                        fullWidth
                        label="Рост"
                        name="height"
                        onChange={this.handleChange}
                        value={this.state.height}
                        variant="outlined"
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <TextField
                        fullWidth
                        label="Вес"
                        name="weight"
                        onChange={this.handleChange}
                        value={this.state.weight}
                        variant="outlined"
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <BaseLookup
                        id="idCountryBorn"
                        label="Страна"
                        value={this.state.idCountryBorn}
                        onChange={this.handleChange}
                        data={this.state.countries}
                        name="idCountryBorn" variant="outlined"
                        onButtonAddClick={() => this.btnAddCountryClick(this)}
                        onButtonEditClick={(id) => this.btnEditCountryClick(this, id)} >


                        <Dialog open={this.state.openidCountryPanel} onClose={() => this.handleClose()} aria-labelledby="form-dialog-title">
                          <DialogTitle id="form-dialog-title">Страна</DialogTitle>
                          <DialogContent>
                            <CountryAddEditView

                              onBtnCancelClick={() => {
                                this.setState({ openidCountryPanel: false });
                              }}

                              onBtnOkClick={(id) => {
                                this.reloadCountries();
                                this.onIdCountryChanged(this, id);
                              }}

                              openPanel={this.state.openidCountryPanel}
                              id={this.state.currentId}
                              isPopup={true}
                            />

                          </DialogContent>
                        </Dialog>

                      </BaseLookup>
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <TextField
                        fullWidth
                        label="Начало дня"
                        type="time"
                        name="workDayStart"
                        onChange={this.handleChange}
                        value={this.state.workDayStart}
                        variant="outlined"
                      />
                    </Grid>

                  </Grid>
                </CardContent>

              </Card>
              <Divider />
              <Card>
                <CardContent>
                  <AddressListView
                    openPanel={this.state.openidCountryPanel}
                    personId={this.state.id}
                  />
                </CardContent>
              </Card>
              <Divider />
              <Card>
                <CardContent>
                  <CitizenshipListView
                    openPanel={this.state.openidCountryPanel}
                    personId={this.state.id}
                  />
                </CardContent>
              </Card>

              <Box display="flex" justifyContent="flex-end" p={2}>

                <Box m={2}>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => {
                      var data = {
                        id: this.state.id - 0,
                        fullName: this.state.fullName,
                        strDateBirth: this.state.dateBirth,
                        hasAuto: !!this.state.hasAuto,
                        height: this.state.height - 0,
                        weight: this.state.weight - 0,
                        idCountryBorn: this.state.idCountryBorn - 0,
                        strWorkDayStart: this.state.workDayStart
                      };

                      fetch('/Person/AddOrUpdate', {
                        method: "POST",
                        body: JSON.stringify(data),
                        headers: { "Content-type": "application/json; charset=UTF-8" }
                      })
                        .then(response => response.json())
                        .then(json => {
                          if (this.props.onBtnOkClick != null) {
                            var id = this.state.id;
                            this.props.onBtnOkClick(id);
                            console.log(json)
                          } else {
                            this.props.navigate('/app/person', { replace: true })
                          }
                        })
                        .catch(err => console.log(err));
                    }}
                  >
                    Сохранить
                  </Button>
                </Box>

                <Box m={2}>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => {
                      if (this.props.onBtnCancelClick != null) {
                        this.props.onBtnCancelClick();
                      } else {
                        this.props.navigate('/app/person', { replace: true })
                      }
                    }}
                  >
                    Отмена
                  </Button>
                </Box>

              </Box>

            </form>
          </Grid>
        </Container>
      </Page>
    );
  }
};

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

function WithAllProps(props) {
  let navigate = useNavigate();
  let query = useQuery();
  let classes = useStyles();
  return <PersonAddEditView {...props} navigate={navigate} classes={classes} idParam={query.get("id")} />
}

export default WithAllProps
