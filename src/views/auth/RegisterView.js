import React, { useState, useEffect } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Checkbox,
  Container,
  FormHelperText, FormControlLabel,
  Link,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import AsyncSelect from "react-select/async";
import { useAsync } from "react-async"

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));


function formatDate(date) {
  return new Date(date).toLocaleDateString()
}

const options = [
  { value: 1, label: 'Chocolate' },
  { value: 2, label: 'Strawberry' },
  { value: 3, label: 'Vanilla' },
];

const loadOptions = (inputValue, callback) => {
  var url = '/Country/GetAll';
  fetch(url)
    .then(res => res.json())
    .then(json => {
      var temp = json.map(function (x) {
        return { value: x.id, label: x.name };
      });
      callback(temp);
    });
};

const RegisterView = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [selectedOption, setSelectedOption] = useState(null);

  return (
    <Page
      className={classes.root}
      title="Register"
    >
      <Box>
        <Container maxWidth="sm">
          <Formik
            initialValues={{
              username: '',
              firstName: '',
              lastName: '',
              password: '',
              hasAuto: false,
              dateBirth: '2020-01-01',
              height: 0,
              weight: 0.0,
              workDayStart: '00:00',
            }}
            validationSchema={
              Yup.object().shape({
                username: Yup.string().max(255).required('username is required'),
                firstName: Yup.string().max(255).required('First name is required'),
                lastName: Yup.string().max(255).required('Last name is required'),
                password: Yup.string().min(6).max(255).required('password is required'),
                dateBirth: Yup.date().min(new Date(1900, 1)).max(new Date()).required('date is required'),
                height: Yup.number().required().positive().integer(),
                weight: Yup.number().required().positive(),
              })
            }
            onSubmit={(values, actions) => {

              var today = new Date();
              var hourMinute = values.workDayStart.split(':');
              var workDayStart = new Date(today.setHours(parseInt(hourMinute[0]), parseInt(hourMinute[1]), 0));

              var body = {
                username: values.username,
                password: values.password,
                fullName: values.firstName + ' ' + values.lastName,
                dateBirth: new Date(values.dateBirth),
                hasAuto: values.hasAuto,
                height: values.height,
                weight: values.weight,
                workDayStart: workDayStart,
                countryId: selectedOption.value
              };

              fetch('/api/Authenticate/Register', {
                method: "POST",
                body: JSON.stringify(body),
                headers: { "Content-type": "application/json; charset=UTF-8" }
              })
                .then(response => {
                  response.json();
                })
                .then(json => {
                  navigate('/login', { replace: true });
                }).catch(err => {
                  console.log(err);
                });

            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values
            }) => (
                <form onSubmit={handleSubmit}>
                  <Box mb={3}>
                    <Typography
                      color="textPrimary"
                      variant="h2"
                    >
                      Create new account
                    </Typography>
                    <Typography
                      color="textSecondary"
                      gutterBottom
                      variant="body2"
                    >
                      Use your username to create new account
                  </Typography>
                  </Box>
                  <TextField
                    error={Boolean(touched.firstName && errors.firstName)}
                    fullWidth
                    helperText={touched.firstName && errors.firstName}
                    label="First name"
                    margin="normal"
                    name="firstName"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.firstName}
                    variant="outlined"
                  />
                  <TextField
                    error={Boolean(touched.lastName && errors.lastName)}
                    fullWidth
                    helperText={touched.lastName && errors.lastName}
                    label="Last name"
                    margin="normal"
                    name="lastName"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.lastName}
                    variant="outlined"
                  />
                  <TextField
                    error={Boolean(touched.username && errors.username)}
                    fullWidth
                    helperText={touched.username && errors.username}
                    label="Username"
                    margin="normal"
                    name="username"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.username}
                    variant="outlined"
                  />
                  <TextField
                    error={Boolean(touched.password && errors.password)}
                    fullWidth
                    helperText={touched.password && errors.password}
                    label="Password"
                    margin="normal"
                    name="password"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="password"
                    value={values.password}
                    variant="outlined"
                  />

                  <TextField
                    error={Boolean(touched.height && errors.height)}
                    fullWidth
                    helperText={touched.height && errors.height}
                    label="height"
                    margin="normal"
                    name="height"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.height}
                    type={"number"}
                    variant="outlined"
                  />

                  <TextField
                    error={Boolean(touched.weight && errors.weight)}
                    fullWidth
                    helperText={touched.weight && errors.weight}
                    label="weight"
                    margin="normal"
                    name="weight"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.weight}
                    type={"number"}
                    variant="outlined"
                  />

                  <TextField
                    error={Boolean(touched.dateBirth && errors.dateBirth)}
                    fullWidth
                    label="dateBirth"
                    margin="normal"
                    name="dateBirth"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.dateBirth}
                    type={"date"}
                    variant="outlined"
                  />

                  <TextField
                    error={Boolean(touched.workDayStart && errors.workDayStart)}
                    fullWidth
                    helperText={touched.workDayStart && errors.workDayStart}
                    label="workDayStart"
                    margin="normal"
                    name="workDayStart"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.workDayStart}
                    type={"time"}
                    variant="outlined"
                  />

                  <FormControlLabel
                    control={<Checkbox checked={values.hasAuto} onBlur={handleBlur} onChange={handleChange} name="hasAuto" />}
                    label="hasAuto"
                  />

                  <AsyncSelect
                    onChange={setSelectedOption}
                    cacheOptions
                    loadOptions={loadOptions}
                    defaultOptions
                  />
                  {/*
                   * <Box
                    alignItems="center"
                    display="flex"
                    ml={-1}
                  >
                    <Checkbox
                      checked={values.hasAuto}
                      name="hasAuto"
                      onChange={handleChange}
                    />
                    <Typography
                      color="textSecondary"
                      variant="body1"
                    >
                      I have read the
                    {' '}
                      <Link
                        color="primary"
                        component={RouterLink}
                        to="#"
                        underline="always"
                        variant="h6"
                      >
                        Terms and Conditions
                    </Link>
                    </Typography>
                  </Box>
                  {Boolean(touched.hasAuto && errors.hasAuto) && (
                    <FormHelperText error>
                      {errors.hasAuto}
                    </FormHelperText>
                  )}*/}
                  <Box my={2}>
                    <Button
                      color="primary"
                      disabled={isSubmitting}
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                    >
                      Sign up now
                  </Button>
                  </Box>
                  <Typography
                    color="textSecondary"
                    variant="body1"
                  >
                    Have an account?
                  {' '}
                    <Link
                      component={RouterLink}
                      to="/login"
                      variant="h6"
                    >
                      Sign in
                  </Link>
                  </Typography>
                </form>
              )}
          </Formik>
        </Container>
      </Box>
    </Page>
  );
};

export default RegisterView;
