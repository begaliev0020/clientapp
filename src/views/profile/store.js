import translit from './translit';

const data = {
  editMode: false,
  language: {
    ru: {
      about: {
        firstName: 'Романова',
        lastName: 'Екатерина',
        patronymic: 'Николаевна',
        birthday: '1990-01-13',
        gender: 'женский',
        email: 'kshitighelani@gmail.com',
        phone: '123 456 7890',
        profession: 10,
        rankings: '7'
      },
      staticWords: {
        changePhoto: 'Изменить фото',
        editProfile: 'Изменить профиль',
        cancel: 'Отменить изменения',
        changeLanguage: 'Изменить язык',
        rankings: 'Рейтинг',
        about: 'Обо мне',
        timeline: 'График',
        name: 'Имя',
        birthday: 'День рождения',
        gender: 'Пол',
        email: 'email',
        phone: 'Тел. номер',
        profession: 'Специальность',
        firstName: 'Фамилия',
        lastName: 'Имя',
        patronymic: 'Отчество',
        female: 'женский',
        male: 'мужчина',
        saveEdit: 'Сохранить изменения'
      },
      // professions: [
      //   'Учитель',
      //   'Доктор',
      //   'Пожарный'
      // ],
      professions: [
        { type: 'Спортсмен', id: 0 },
        { type: 'Учитель', id: 1 },
        { type: 'Доктор', id: 2 },
        { type: 'Пожарный', id: 3 },
        { type: 'Программист', id: 4 },
        { type: 'Водитель', id: 5 },
        { type: 'Дизайнер', id: 6 },
        { type: 'Строитель', id: 7 },
        { type: 'Таксист', id: 8 },
        { type: 'Директор', id: 9 },
        { type: 'Адвокат', id: 10 },
        { type: 'Актер', id: 11 },
        { type: 'Веб разработчик', id: 12 }
      ]
    },

    en: {
      about: {
        firstName: 'Romanova',
        lastName: 'Ekaterina',
        patronymic: 'Nikolaevna',
        birthday: '1990-01-13',
        gender: 'female',
        email: 'kshitighelani@gmail.com',
        phone: '123 456 7890',
        profession: 10,
        rankings: '7'
      },
      staticWords: {
        changePhoto: 'Change photo',
        editProfile: 'Edit profile',
        cancel: 'cancel edit profile',
        changeLanguage: 'Change language',
        rankings: 'RANKINGS',
        about: 'about',
        timeline: 'timeline',
        name: 'name',
        birthday: 'birthday',
        gender: 'gender',
        email: 'email',
        phone: 'phone',
        profession: 'profession',
        firstName: 'First Name',
        lastName: 'Last Name',
        patronymic: 'Patronymic',
        female: 'female',
        male: 'male',
        saveEdit: 'Save Edit'
      },
      professions: [
        { type: 'Sportsman', id: 0 },
        { type: 'Teacher', id: 1 },
        { type: 'Doctor', id: 2 },
        { type: 'Fireman', id: 3 },
        { type: 'Programmer', id: 4 },
        { type: 'Driver', id: 5 },
        { type: 'Designer', id: 6 },
        { type: 'Builder', id: 7 },
        { type: 'Taxi driver', id: 8 },
        { type: 'Director', id: 9 },
        { type: 'Lawyer', id: 10 },
        { type: 'Actor', id: 11 },
        { type: 'Web Developer and Designer', id: 12 }
      ]
    }
  }
};

export const changeData = (changedData) => {
  if (changedData.language == 'ru') {

    data.language.ru.about.lastName = changedData.lastName;
    data.language.ru.about.firstName = changedData.firstName;
    data.language.ru.about.patronymic = changedData.patronymic;
    data.language.ru.about.gender = changedData.gender;
    data.language.ru.about.profession = changedData.profession;
    data.language.en.about.profession = changedData.profession;
    data.language.ru.about.email = changedData.email;
    data.language.en.about.email = changedData.email;
    data.language.ru.about.phone = changedData.phone;
    data.language.en.about.phone = changedData.phone;
    data.language.ru.about.birthday = changedData.birthday;
    if (changedData.gender == 'женский') data.language.en.about.gender = 'female';
    else data.language.en.about.gender = 'male';
  } else {

    data.language.en.about.lastName = changedData.lastName;
    data.language.en.about.firstName = changedData.firstName;
    data.language.en.about.patronymic = changedData.patronymic;
    data.language.en.about.birthday = changedData.birthday;
    data.language.ru.about.profession = changedData.profession;
    data.language.en.about.profession = changedData.profession;
    data.language.en.about.gender = changedData.gender;
    data.language.en.about.email = changedData.email;
    data.language.ru.about.email = changedData.email;
    data.language.en.about.phone = changedData.phone;
    data.language.ru.about.phone = changedData.phone;
    if (changedData.gender == 'female') data.language.ru.about.gender = 'женский';
    else data.language.ru.about.gender = 'мужчина';
  }
};

export const changeEditMode = () => {
  data.editMode = !data.editMode;
};

const takeDataWithLanguage = (language) => {
  if (language === 'ru') return data.language.ru;
  else if (language === 'en') return data.language.en;
};

export const takeEditMode = () => {
  return data.editMode;
};

export default takeDataWithLanguage;
