import React from 'react';

import img from './image.jpg';
import { makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  img: {
    width: '100%',
    // height: '100%',
  },
  imgBlock: {
    display: 'flex',
    flexDirection: "column",
    justifyContent: "space-around",
    height: '300px',
    [theme.breakpoints.down('xs')]: {
      height: '20%',
    },
    ['@media (max-width:1105px)']: {
      height: '450px'
    },
  },
  file: {
    alignContent: 'center',
  },
  button: {
    width: '100%',
  }
}));

const Image = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.imgBlock}>
      <div>
        <img className={classes.img} src={img} alt='df' />
      </div>
      <div>
        <Button
          variant="contained"
          component="label"
          className={classes.button}
        >
          {props.data.staticWords.changePhoto}
          <input
            type="file"
            hidden
          />
        </Button>
      </div>
    </div>
  );
};

export default Image;
