import React from 'react';
import { makeStyles } from '@material-ui/core';
import { Icon } from '@iconify/react';
import genderFemale from '@iconify-icons/mdi/gender-female';
import genderMale from '@iconify-icons/mdi/gender-male';

const useStyles = makeStyles((theme) => ({
  row: {
    display: 'flex',
    marginBottom: '20px',
    justifyContent: 'space-around'
  },
  item: {
    width: '40%',
    fontSize: '20px',
    fontWeight: 500,
    margin: '10px'
  },
}));


const About = (props) => {
  const classes = useStyles();

  if (props.data.gender == 'female' || props.data.gender == 'женский'){
    props.data.genderi = <Icon icon={genderFemale} />;
  }else{
    props.data.genderi = <Icon icon={genderMale} />;
  }

  return (
    <div className={classes.main}>
      <div className={classes.row}>
        <div className={classes.item}>
          {props.data.staticWords.name}
        </div>
        <div className={classes.item}>
          {props.data.about.lastName}
        </div>
      </div>

      <div className={classes.row}>
        <div className={classes.item}>
          {props.data.staticWords.birthday}
        </div>
        <div className={classes.item}>
          {props.data.about.birthday}
        </div>
      </div>
      <div className={classes.row}>
        <div className={classes.item}>
          {props.data.staticWords.gender}
        </div>
        <div className={classes.item}>
          {props.data.genderi} {props.data.about.gender}
        </div>
      </div>
      <div className={classes.row}>
        <div className={classes.item}>
          {props.data.staticWords.email}
        </div>
        <div className={classes.item}>
          {props.data.about.email}
        </div>
      </div>
      <div className={classes.row}>
        <div className={classes.item}>
          {props.data.staticWords.phone}
        </div>
        <div className={classes.item}>
          {props.data.about.phone}
        </div>
      </div>
      <div className={classes.row}>
        <div className={classes.item}>
          {props.data.staticWords.profession}
        </div>
        <div className={classes.item}>
          {props.data.professions[props.data.about.profession].type}
        </div>
      </div>
    </div>
  )
}


export default About;
