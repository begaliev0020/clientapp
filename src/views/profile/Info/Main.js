
import React from 'react';
import FullWidthTabs from './FullWidthTabs';
import EditForm from './EditForm';
import changeEditMode from './../store'


class Main extends React.Component {
  render() {

    return (
      <>
        {
          !this.props.editMode && <FullWidthTabs data={this.props.data} />
        }
        {
          this.props.editMode && <EditForm data={this.props.data} states={this.props.states}/>
        }
      </>
    );
  }
}

export default Main;
