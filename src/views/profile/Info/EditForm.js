import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  InputLabel, MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField
} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
// import BirthdayEdit from './Edit/BirthdayEdit';
import { changeData } from './../store'

const useStyles =(theme) => ({
  form: {
    display: 'flex',
  },
  root: {
    '& .MuiTextField-root': {
      margin: '10px 50px',
      width: '200px',
    }
  },
  gender: {
    float: 'right',
    margin: '15px 70px'
  },
  button: {
    margin: '30px 50px',
    float: 'right'
  },
  radio: {
    margin: '0'
  },
  formControl: {
    margin: theme.spacing(1),
    marginLeft: theme.spacing(7),
    minWidth: 120,
    width: 400
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  }
});

class EditForm extends React.Component {
  constructor(props) {
    super(props);
    this.professions = this.props.data.professions
    this.state = {
      gender: this.checkLanguageGender(this.props.data.about.gender),
      firstName: this.props.data.about.firstName,
      lastName: this.props.data.about.lastName,
      patronymic: this.props.data.about.patronymic,
      email: this.props.data.about.email,
      birthday: this.props.data.about.birthday,
      phone: this.props.data.about.phone,
      profession: this.props.data.about.profession,
    }
    this.handleChange = this.handleChangeFunc.bind(this)
    this.firstNameChange = this.firstNameChangeFunc.bind(this)
    this.lastNameChange = this.lastNameChangeFunc.bind(this)
    this.patronymicChange = this.patronymicChangeFunc.bind(this)
    this.emailChange = this.emailChangeFunc.bind(this)
    this.phoneChange = this.phoneChangeFunc.bind(this)
    this.birthdayChange = this.birthdayChangeFunc.bind(this)
    this.profhandleChange = this.profhandleChangeFunc.bind(this)
    this.onClickButton = this.onClickButtonFunc.bind(this)
  }

  checkLanguageGender(gender){
    if (gender == 'мужчина'){
      return 'male'
    }else if(gender == 'женский'){
      return 'female'
    }else{
      return gender
    }
  }

  onClickButtonFunc(){
    let data = {
      language: this.props.states.language,
      lastName: this.state.lastName,
      firstName: this.state.firstName,
      patronymic: this.state.patronymic,
      email: this.state.email,
      phone: this.state.phone,
      gender: this.state.gender,
      profession: this.state.profession,
      birthday: this.state.birthday
    }
    changeData(data)
    this.props.states.activateMode()
  }

  profhandleChangeFunc(event){
    this.setState({
      profession: this.professions[event.target.value].id
    })
  }

  handleChangeFunc(event){
    this.setState({
      gender: event.target.value
    })
  }

  firstNameChangeFunc(event){
    this.setState({
      firstName: event.target.value
    })
  }

  lastNameChangeFunc(event){
    this.setState({
      lastName: event.target.value
    })
  }
  patronymicChangeFunc(event){
    this.setState({
      patronymic: event.target.value
    })
  }

  birthdayChangeFunc(event){
    this.setState({
      birthday: event.target.value
    })
  }

  emailChangeFunc(event){
    this.setState({
      email: event.target.value
    })
  }

  phoneChangeFunc(event){
    this.setState({
      phone: event.target.value
    })
  }


  render() {
    const { classes } = this.props;

    return (
      <div className={classes.form}>
        <form className={classes.root} noValidate autoComplete='off'>

          <FormControl className={classes.gender} component='fieldset'>
            <FormLabel component='legend'>{this.props.data.staticWords.gender}</FormLabel>
            <RadioGroup aria-label='gender' name='gender1' value={this.state.gender} onChange={this.handleChange}>
              <FormControlLabel className={classes.radio} value='female' control={<Radio />} label={this.props.data.staticWords.female} />
              <FormControlLabel className={classes.radio} value='male' control={<Radio />} label={this.props.data.staticWords.male} />
            </RadioGroup>
          </FormControl>
          <TextField
            size='medium'
            className={classes.textField}
            id='outlined-multiline-flexible'
            label={this.props.data.staticWords.firstName}
            value={this.state.firstName}
            variant='outlined'
            onChange={this.firstNameChange}
          />
          <TextField
            className={classes.textField}
            id='outlined-multiline-flexible'
            label={this.props.data.staticWords.lastName}
            value={this.state.lastName}
            variant='outlined'
            onChange={this.lastNameChange}
          />
          <TextField
            className={classes.textField}
            id='outlined-multiline-flexible'
            label={this.props.data.staticWords.patronymic}
            value={this.state.patronymic}
            variant='outlined'
            onChange={this.patronymicChange}
          />

          {/*<BirthdayEdit data={this.state} click={this.birthdayChange}/>*/}

          <TextField
            id='date'
            label='Birthday'
            type='date'
            defaultValue={this.state.birthday}
            className={classes.textField}
            onChange={this.birthdayChange}
            InputLabelProps={{
              shrink: true
            }}
          />

          <TextField
            className={classes.textField}
            id='outlined-multiline-flexible'
            label={this.props.data.staticWords.email}
            value={this.state.email}
            variant='outlined'
            onChange={this.emailChange}
          />
          <TextField
            className={classes.textField}
            id='outlined-multiline-flexible'
            label={this.props.data.staticWords.phone}
            value={this.state.phone}
            variant='outlined'
            onChange={this.phoneChange}
          />
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">{this.professions[this.state.profession].type}</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={this.state.profession}
              onChange={this.profhandleChange}
            >
              {this.professions.map((profession)=>
                <MenuItem value={profession.id}>{profession.type}</MenuItem>
              )}
            </Select>
          </FormControl>
          <Button className={classes.button}
                  variant="contained"
                  color="primary"
                  type='submit'
                  startIcon={<SaveIcon />}
                  onClick={this.onClickButton}
                  type="button"
          >
            {this.props.data.staticWords.saveEdit}
          </Button>
        </form>
      </div>
    );
  }
}


export default withStyles(useStyles)(EditForm);
