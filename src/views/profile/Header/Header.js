import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import EditButton from './EditButton';
import ChangeLanguage from './ChangeLanguage';

const useStyles = (theme) => ({
  header: {
    position: 'relative',
    height: '300px',
    display: 'flex',
    ['@media (max-width:1105px)']: {
      display: 'block',
      height: '450px'
    },
  },
  name: {
    fontFamily: 'arial',
    padding: '20px',
    fontSize: '40px',
  },
  profession: {
    fontFamily: '"inherit", sans-serif',
    color: '#0062cc',
    fontSize: '20px',
    padding: '20px'
  },
  rank: {
    padding: '20px',
    fontWeight: '400',
    color: '#818182',
    fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"'
  },
  rankValue: {
    color: 'black',
    fontWeight: '700'
  },
  info: {
    float: 'left',
  },
});


class Header extends Component {

  render() {
    const { classes } = this.props;
    const data = this.props.data.about;
    const staticWords = this.props.data.staticWords;
    const name = data.firstName+ ' ' + data.lastName + ' ' + data.patronymic;

    return (
      <div className={classes.header}>
        <div className={classes.info}>
          <div className={classes.name}>{name}</div>
          <div className={classes.profession}>{this.props.data.professions[data.profession].type}</div>
          <div>
            <p className={classes.rank}>
              {staticWords.rankings}:
              <span className={classes.rankValue}> {data.rankings}/10</span>
            </p>
          </div>
        </div>
        <div className={classes.buttons}>

          <EditButton states={this.props.states} data={staticWords}/>
          <ChangeLanguage states={this.props.states} data={staticWords}/>

        </div>
      </div>
    );
  }
}

export default withStyles(useStyles)(Header);
