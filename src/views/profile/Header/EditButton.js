import React from 'react';
import Button from '@material-ui/core/Button';
import SettingsIcon from '@material-ui/icons/Settings';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  button: {
    margin: '15px 20px',
    float: 'right',
  }
}));

const EditButton = (props) => {
  const classes = useStyles();

  return (
    <div>
      <Button
        onClick={props.states.activateMode}
        variant='contained'
        color='default'
        className={classes.button}
        startIcon={<SettingsIcon />}
      >
      {
        !props.states.editMode && <>{props.data.editProfile}</>
      }
      {
        props.states.editMode && <>{props.data.cancel}</>
      }
      </Button>
    </div>
  );
};

export default EditButton;
