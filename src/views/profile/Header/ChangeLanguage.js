import React from 'react';
import Button from '@material-ui/core/Button';
import SettingsIcon from '@material-ui/icons/Settings';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  button: {
    float: 'right',
    margin: '15px 20px'
  }
}));

const EditButton = (props) => {
  const classes = useStyles();

  return (
    <div>
      <Button
        onClick={props.states.changeLanguage}
        variant='contained'
        color='default'
        className={classes.button}
        startIcon={<SettingsIcon />}
      >
       {props.data.changeLanguage}
      </Button>
    </div>
  );
};

export default EditButton;
