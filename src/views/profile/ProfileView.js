import { Grid, Container, withStyles } from '@material-ui/core';
import Image from './Image/Image';
import Header from './Header/Header';
import React, { Component, useState } from 'react';
import Page from '../../components/Page';
import takeDataWithLanguage from './store';
import Main from './Info/Main';
import { takeEditMode, changeEditMode } from './store'


const useStyles =(theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  main: {
    height: '800px',
    borderRadius: '10px'
  },
  grid: {
    backgroundColor: 'white',
    borderRadius: '5px',
    padding: '12px',
    border: '1px solid #ccc'
  }
});

class ProfileView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      activateMode: this.activateEditMode.bind(this),
      language: 'ru',
      changeLanguage: this.changeLanguage.bind(this),
    };
  }

  changeLanguage(){
    if (this.state.language == 'ru') {
      this.setState({language: 'en'})
    }else{
      this.setState({language: 'ru'})
    }
  }

  activateEditMode() {
    this.setState({ editMode: !this.state.editMode });
    changeEditMode();
  }


  render() {
    const { classes } = this.props;
    const data = takeDataWithLanguage(this.state.language);
    return (
      <Page
        className={classes.root}
        title='Account'
      >
        <Container>
          <div className={classes.main}>
            <Grid container spacing={3}>
              <Grid item
                    xl={4}
                    lg={4}
                    md={5}
                    sm={6}
                    xs={12}>
                <div className={classes.grid}>

                  <Image data={data}/>

                </div>
              </Grid>
              <Grid item
                    xl={8}
                    lg={8}
                    md={7}
                    sm={6}
                    xs={12}>
                <div className={classes.grid}>

                  <Header data={data} states={this.state}/>

                </div>
              </Grid>
              <Grid item
                    xl={12}
                    lg={12}
                    md={12}
                    sm={12}
                    xs={12}>
                <div className={classes.grid}>

                  <Main data={data} editMode={takeEditMode()} states={this.state}/>

                </div>
              </Grid>
            </Grid>
          </div>
        </Container>
      </Page>
    );
  }
}


export default withStyles(useStyles)(ProfileView);
