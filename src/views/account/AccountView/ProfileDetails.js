import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles
} from '@material-ui/core';

const states = [
  {
    value: 'alabama',
    label: 'Alabama'
  },
  {
    value: 'new-york',
    label: 'New York'
  },
  {
    value: 'san-francisco',
    label: 'San Francisco'
  }
];

const useStyles = makeStyles(() => ({
  root: {}
}));

const ProfileDetails = ({ className, data, cancelHandler, ...rest }) => {
  const classes = useStyles();
  const [values, setValues] = useState({
    id: data.id,
    name: data.name,
    description: data.description,
    code: data.code,
  });


  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  return (
    <form
      autoComplete="off"
      noValidate
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card>
        <CardHeader
          //subheader="The information can be edited"
          title="Страна"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={12}
              xs={12}
            >
              <TextField
                fullWidth
                label="Название"
                name="name"
                onChange={handleChange}
                required
                value={values.name}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={12}
              xs={12}
            >
              <TextField
                fullWidth
                label="Описание"
                name="description"
                onChange={handleChange}
                value={values.description}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={12}
              xs={12}
            >
              <TextField
                fullWidth
                label="Код"
                name="code"
                onChange={handleChange}
                value={values.code}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          <Box m={2}>
            <Button
              color="primary"
              variant="contained"
              onClick={() => {
                fetch('/Country/AddOrUpdate', {
                  method: "POST",
                  body: JSON.stringify(values),
                  headers: { "Content-type": "application/json; charset=UTF-8" }
                })
                  .then(response => response.json())
                  .then(json => console.log(json)).catch(err => console.log(err));
                cancelHandler();
              }}
            >
              Save details
            </Button>
          </Box>
          <Box m={2}>
            <Button
              color="primary"
              variant="contained"
              onClick={cancelHandler}
            >
              Cancel
            </Button>
          </Box>


        </Box>
      </Card>
    </form>
  );
};

ProfileDetails.propTypes = {
  className: PropTypes.string,
  cancelHandler: PropTypes.func
};

export default ProfileDetails;
