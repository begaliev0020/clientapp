import React from 'react';
import {
  Container,
  Grid,
  makeStyles, Button
} from '@material-ui/core';
import Page from 'src/components/Page';
import Profile from './Profile';
import ProfileDetails from './ProfileDetails';
import PropTypes from 'prop-types';
import { useAsync } from "react-async"

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const fetchData = async ({ itemId }) => {
  if (itemId == null || itemId == 0) return { id: 0, name: null, code: null, description: null };
  const response = await fetch('/Country/GetOneById?id='+itemId)
  if (!response.ok) throw new Error(response.status)
  return response.json()
}

const Account = ({ handler, openPanel, id}) => {

  const classes = useStyles();

  const { data, error } = useAsync({ promiseFn: fetchData, itemId: id})
  if (!openPanel) return null;
  if (data == null) return null;
  return (
    <Page
      className={classes.root}
      title="Account"
    >
      <Container maxWidth="lg">
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={4}
            md={6}
            xs={12}
          >
            <Profile />
          </Grid>
          <Grid
            item
            lg={8}
            md={6}
            xs={12}
          >
          </Grid>

          <ProfileDetails data={ data } cancelHandler={handler} />

        </Grid>
      </Container>
    </Page>

  );
};

Account.propTypes = {
  handler: PropTypes.func
};

export default Account;
