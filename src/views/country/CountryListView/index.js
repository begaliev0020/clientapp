import React, { useState } from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import PageGrid from 'src/components/PageGrid';

class CountryListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openPanel: false,
      currentId: 0,
      data: []
    };
  }

  onItemDeleted() {
    this.loadCountryList();
  }

  loadCountryList() {
    var url = '/Country/GetAll';
    userApiClient(url)
      .then(json => this.setState({ data: json }));
  }

  componentDidMount() {
    this.loadCountryList();
  }

  render() {
    const columns = [
      {
        name: "name", //lower case Id_Coutnry -> idcountry, менять в эрвине
        label: "Название",
        options: {
          filter: true,
        }
      },
      {
        label: "Описание",
        name: 'description',
        options: {
          filter: true,
        }
      },
      {
        label: "Код",
        name: 'code',
        options: {
          filter: true,
        }
      },
    ];
    return (
      <Page className={this.props.classes.root}  title="Страны">

        <Container maxWidth={false}>
          <PageGrid
            title="Страны 1"
            subtitle="Страны 2"
            onItemDeleted={() => this.onItemDeleted(this)}
            columns={columns}
            data={this.state.data}
            controllerName="country" />
        </Container>
      </Page>
    );
  }

};


const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

function WithAllProps(props) {
  let classes = useStyles();
  return <CountryListView {...props} classes={classes} />
}

export default WithAllProps
