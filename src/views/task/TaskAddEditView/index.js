import React from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import { useNavigate } from 'react-router-dom';
import { useParams, useLocation } from "react-router";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  Checkbox,
  Select,
  MenuItem,
  InputBase,
  NativeSelect,
  FormControl,
  InputLabel,
  Container
} from '@material-ui/core';
import Page from 'src/components/Page';

class CountryAddEditView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {

      //все поля из дто
      id: 0,
      title: '',
      body: '',
      IdExecutor: 2,
      IdReporter: 14,
      IdState: 1

      //другие поля
    };
  }

  componentDidMount() {
    //параметр, пришедший во всплывающее окно (с лукапа)
    var itemId = this.props.id;

    //если параметр пустой, то проверяем строку браузера - вдруг открываем не в окне (со списка)
    if (!this.props.isPopup && (itemId == null || itemId == 0)) {
      itemId = this.props.idParam / 1;
    }


    if (itemId == null || itemId == 0) {

      this.setState({
        //поля из дтошки
        id: 0, title: '', body: ''
        //должно быть без запятой в конце
      });
      return;
    }
    
    var url = '/Task/GetOneById?id=' + itemId;
    userApiClient(url)
      .then(json => this.setState({
        //поля из дтошки
        id: json.id, title: json.title, body: json.body
        //должно быть без запятой в конце
      }));
  }

  //ловим изменения, сделанные пользователем, и кидаем в модельку
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
      <Page className={this.props.classes.root} title="Задача">
        <Container maxWidth="lg">
          <Grid container spacing={3}>
            <Card>
              <CardHeader title="Задача" />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>

                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Название"
                      name="title"
                      onChange={this.handleChange}
                      required
                      value={this.state.title}
                      variant="outlined"
                    />
                  </Grid>

                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Описание"
                      name="body"
                      onChange={this.handleChange}
                      value={this.state.body}
                      variant="outlined"
                    />
                  </Grid>

                </Grid>
              </CardContent>
            </Card>

            <Divider />
            <Box display="flex" justifyContent="flex-end" p={2}>
              <Box m={2}>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={() => {
                    fetch('/Task/AddOrUpdate', {
                      method: "POST",
                      body: JSON.stringify(this.state),
                      headers: { "Content-type": "application/json; charset=UTF-8" }
                    })
                      .then(response => response.json())
                      //.then(json => this.setState({ data: json }))
                      .then(json => {
                        if (this.props.onBtnOkClick != null) {
                          var id = json.id;
                          this.props.onBtnOkClick(id);
                          console.log(json)
                        } else {
                          this.props.navigate('/app/task', { replace: true })
                        }

                      })
                      .catch(err => console.log(err));

                  }}
                >
                  Сохранить
            </Button>
              </Box>
              <Box m={2}>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={() => {
                    if (this.props.onBtnCancelClick != null) {
                      this.props.onBtnCancelClick();
                    } else {
                      this.props.navigate('/app/task', { replace: true })
                    }
                  }
                  }
                >
                  Отмена
            </Button>
              </Box>
            </Box>
          </Grid>
        </Container>
      </Page>
    );
  }
};

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

function WithAllProps(props) {
  let navigate = useNavigate();
  let query = useQuery();
  let classes = useStyles();
  return <CountryAddEditView {...props} navigate={navigate} classes={classes} idParam={query.get("id")} />
}

export default WithAllProps
