import React, { useState } from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import PageGrid from 'src/components/PageGrid';

class TaskListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openPanel: false,
      currentId: 0,
      data: []
    };
  }

  onItemDeleted() {
    this.loadTaskList();
  }

  loadTaskList() {
    var url = '/Task/GetAllMy';
    userApiClient(url)
      .then(json => this.setState({ data: json }));
  }

  componentDidMount() {
    this.loadTaskList();
  }

  render() {
    const columns = [
      {
        name: "title", //lower case Id_Coutnry -> idcountry, менять в эрвине
        label: "Название",
        options: {
          filter: true,
        }
      },
      {
        label: "Описание",
        name: 'body',
        options: {
          filter: true,
        }
      },
    ];
    return (
      <Page className={this.props.classes.root}  title="Задачи">

        <Container maxWidth={false}>
          <PageGrid
            title="Задачи 1"
            subtitle="Задачи 2"
            onItemDeleted={() => this.onItemDeleted(this)}
            columns={columns}
            data={this.state.data}
            controllerName="task" />
        </Container>
      </Page>
    );
  }

};


const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

function WithAllProps(props) {
  let classes = useStyles();
  return <TaskListView  {...props} classes={classes} />
}

export default WithAllProps
