import React, { useState } from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import {
  Box,
  Container,
  Button,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import CitizenshipAddEditView from 'src/views/citizenship/CitizenshipAddEditView';
import PopupGrid from 'src/components/PopupGrid';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


export default class CitizenshipListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      openPanel: false,
      currentId: 0,
      personId: 0,


      id: 0,

      series: '',
      number: '',
      idCountry: 0,
      idPerson: this.props.personId,
      idScannedFile: null,
      scannedFileName: '',
      myFile: null,
    };
  }


  handleChange = (event) => {
    const files = event.target.files;
    if (files != null) {
      this.state.myFile = files[0];
      this.setState({ ['scannedFileName']: event.target.value });
      return;
    }

    this.setState({ [event.target.name]: event.target.value });
  }

  handleClose = () => {
    //setOpen(false);
  };

  onButtonAddEditClick(t, id) {
    t.setState({ currentId: id, openPanel: true });
  }

  loadCitizenships() {
    var url = '/Citizenship/GetByPersonId?id=' + this.props.personId;
    userApiClient(url)
      .then(json => this.setState({ data: json }));
  }

  componentDidUpdate() {
    if (this.props.personId == 0) return;
    if (this.props.personId == this.state.personId) return;
    this.setState({ personId: this.props.personId });
    this.loadCitizenships();
  }
  componentDidMount() {
  }

  render() {
    const columns = [
      {
        name: "series",
        label: "Серия",
        options: {
          filter: true,
        }
      },
      {
        name: 'number',
        label: "Номер",
        options: {
          filter: true,
        }
      },
      {
        name: 'countryName',
        label: "Страна",
        options: {
          filter: true,
        }
      },
    ];
    return (
      <Page title="Гражданство">

        <Container maxWidth={false}>

          <PopupGrid
            title="Гражданства 1"
            subtitle="Гражданства 2"
            onButtonAddEditClick={(id) => this.onButtonAddEditClick(this, id)}
            onItemDeleted={() => this.loadCitizenships(this)}
            columns={columns}
            data={this.state.data}
            controllerName="Citizenship" />

          <Dialog open={this.state.openPanel} onClose={() => this.handleClose()} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Адрес</DialogTitle>
            <DialogContent>

            <CitizenshipAddEditView
                onChange={(event) => this.handleChange(event)}
                onItemDeleted={() => {
                  this.loadAddresss();
                }}
                openPanel={this.state.openPanel}
                id={this.state.currentId}
                personId={this.props.personId}
            />

            </DialogContent>
            <DialogActions>
              <Button
                color="secondary"
                variant="contained"
                onClick={() => {
                  var data = {
                    id: this.state.id,

                    series: this.state.series,
                    number: this.state.number,
                    idCountry: this.state.idCountry,
                    idPerson: this.props.personId,
                    idScannedFile: this.state.idScannedFile,
                    myFile: this.state.myFile
                  };

                  const formData = new FormData();
                  for (var key in data) {
                    if (key == "idScannedFile" && data[key] == null) continue;
                    formData.append(key, data[key]);
                  }

                  fetch('/Citizenship/AddOrUpdateForm', {
                    method: "POST",
                    body: formData,
                  })
                    .then(response => response.json())
                    .then(json => {
                      this.setState({ openPanel: false });

                      this.loadCitizenships();
                    })
                    .catch(err => console.log(err));
                }}
              >
                Сохранить
              </Button>
              <Button
                color="primary"
                variant="contained"
                onClick={() => this.setState({ openPanel: false })}
              >
                Отмена
                  </Button>
            </DialogActions>
          </Dialog>

        </Container>
      </Page>
    );
  }

};


