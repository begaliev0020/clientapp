import React from 'react';
import Page from 'src/components/Page';
import PropTypes from 'prop-types';
import { useAsync } from "react-async"
import { userApiClient } from 'src/components/ApiHelper'
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  Checkbox,
  FormControlLabel,
  Container,
  Select,
  MenuItem,
  InputBase,
  NativeSelect,
  FormControl,
  InputLabel
} from '@material-ui/core';
import BaseLookup from 'src/components/BaseLookup'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import CountryAddEditView from 'src/views/country/CountryAddEditView';

import ClearIcon from '@material-ui/icons/Clear';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

export default class CitizenshipAddEditView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,

      series: '',
      number: '',
      idCountry: 0,
      idPerson: this.props.personId,
      idScannedFile: null,
      scannedFileName: '',
      myFile: null,

      inputKey: Math.random().toString(36),

      openidCountryPanel: false,
      countries: []
    };
  }

  componentDidMount() {


    this.loadCountries();

    var itemId = this.props.id;
    if (itemId == null || itemId == 0) {
      //this.setState({
      //  id: 7,
      //  fullName: '',
      //  dateBirth: '',
      //  hasAuto: false,
      //  height: '',
      //  weight: '',
      //  idCountry: '',
      //  workDayStart: ''
      //});
      return;
    }

    var url = '/Citizenship/GetOneById?id=' + itemId;
    userApiClient(url)
      .then(json => {
        this.setState({
          id: json.id,
          series: json.series,
          number: json.number,
          idCountry: json.idCountry,
          idPerson: json.idPerson,
          idScannedFile: json.idScannedFile,
          scannedFileName: json.scannedFileName,
          myFile: null,
        });

        var event = { target: { name: 'id', value: json.id, } };
        this.props.onChange(event);

        event = { target: { name: 'series', value: json.series, } };
        this.props.onChange(event);

        event = { target: { name: 'idCountry', value: json.idCountry, } };
        this.props.onChange(event);

        event = { target: { name: 'idPerson', value: json.idPerson, } };
        this.props.onChange(event);

        event = { target: { name: 'idScannedFile', value: json.idScannedFile, } };
        this.props.onChange(event);

        event = { target: { name: 'scannedFileName', value: json.scannedFileName, } };
        this.props.onChange(event);
      });

  }

  loadCountries() {
    var url = '/Country/GetAll';
    userApiClient(url)
      .then(json => this.setState({ countries: json }));
  }

  reloadCountries() {
    this.loadCountries();
    this.setState({ openidCountryPanel: false });
  }


  handleChange = (event) => {
    const files = event.target.files;
    if (files != null) {
      this.state.myFile = files[0];
      this.setState({ ['scannedFileName']: event.target.value });
      this.props.onChange(event);
      return;
    }

    this.setState({ [event.target.name]: event.target.value });

    this.props.onChange(event);
  }

  btnAddCountryClick(t) {
    t.setState({ openidCountryPanel: true, currentId: 0 });
  }

  btnEditCountryClick(t, id) {
    t.setState({ openidCountryPanel: true, currentId: id });
  }


  render() {
    return (
      <Page title="Гражданство">
        <Container maxWidth="lg">
          <Grid container spacing={3}>

            <form autoComplete="off" noValidate>
              <Card>
                <CardHeader title="Гражданство" />
                <Divider />
                <CardContent>
                  <Grid container spacing={3}>

                    <Grid
                      item
                      md={12}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Серия"
                        name="series"
                        onChange={this.handleChange}
                        required
                        value={this.state.series}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Номер"
                        name="number"
                        onChange={this.handleChange}
                        value={this.state.number}
                        variant="outlined"
                      />
                    </Grid>

                    <Grid
                      item
                      md={12}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Файл"
                        name="scannedFileName"
                        onChange={this.handleChange}
                        value={this.state.scannedFileName}
                        variant="outlined"
                        InputProps={{
                          readOnly: true,
                          endAdornment: <div><input
                            style={{ display: 'none' }}
                            onChange={this.handleChange}
                            id="raised-button-file"
                            multiple
                            key={this.state.inputKey}
                            type="file"
                          />
                            <React.Fragment>
                              <label htmlFor="raised-button-file">
                                <CloudUploadIcon style={{ cursor: 'pointer' }} />
                              </label>
                              <ClearIcon style={{ cursor: 'pointer' }} onClick={() => {
                                this.state.inputKey = Math.random().toString(36);
                                this.state.scannedFileName = '';

                              }} />
                            </React.Fragment>


                          </div>
                        }}

                      />


                    </Grid>


                    <Grid item md={12} xs={12}>
                      <BaseLookup
                        id="idCountry"
                        label="Страна"
                        value={this.state.idCountry}
                        onChange={this.handleChange}
                        data={this.state.countries}
                        name="idCountry" variant="outlined"
                        onButtonAddClick={() => this.btnAddCountryClick(this)}
                        onButtonEditClick={(id) => this.btnEditCountryClick(this, id)} >

                        <Dialog open={this.state.openidCountryPanel} onClose={() => this.handleClose()} aria-labelledby="form-dialog-title">
                          <DialogTitle id="form-dialog-title">Страна</DialogTitle>
                          <DialogContent>
                            <CountryAddEditView

                              onBtnCancelClick={() => {
                                this.setState({ openidCountryPanel: false });
                              }}

                              onBtnOkClick={(id) => {
                                this.reloadCountries();
                              }}

                              openPanel={this.state.openidCountryPanel}
                              id={this.state.currentId}
                            />

                          </DialogContent>
                        </Dialog>

                      </BaseLookup>
                    </Grid>


                  </Grid>
                </CardContent>

              </Card>
              <Divider />

              {/*
              <Box display="flex" justifyContent="flex-end" p={2}>

                <Box m={2}>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => {
                      var data = {
                        id: this.state.id,

                        series: this.state.series,
                        number: this.state.number,
                        idCountry: this.state.idCountryCitizenship,
                        idPerson: this.state.idPerson,
                        idScannedFile: this.state.idScannedFile,
                        myFile: this.state.myFile
                      };

                      const formData = new FormData();
                      for (var key in data) {
                        if (key == "idScannedFile" && data[key] == null) continue;
                        formData.append(key, data[key]);
                      }

                      fetch('/Citizenship/AddOrUpdateForm', {
                        method: "POST",
                        body: formData,
                      })
                        .then(response => response.json())
                        .then(json => {
                          var id = this.state.id;
                          this.props.onBtnOkClick(id);
                          console.log(json)
                        })
                        .catch(err => console.log(err));
                    }}
                  >
                    Сохранить
                  </Button>
                </Box>

                <Box m={2}>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => this.props.onBtnCancelClick()}
                  >
                    Отмена
                  </Button>
                </Box>

              </Box>
              */}
            </form>
          </Grid>
        </Container>
      </Page>
    );
  }
};
