import React from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import Page from 'src/components/Page';
import PropTypes from 'prop-types';
import { useAsync } from "react-async"
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  Checkbox,
  FormControlLabel,
  Container,
  Select,
  MenuItem,
  InputBase,
  NativeSelect,
  FormControl,
  InputLabel
} from '@material-ui/core';
import BaseLookup from 'src/components/BaseLookup'

import SlidingPanel from 'react-sliding-side-panel';
import CountryAddEditView from 'src/views/country/CountryAddEditView';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class AddressAddEditView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      addressText: '',
      idCountry: 0,
      idRegion: 0,
      idCity: 0,
      idPerson: this.props.personId,

      openidCountryPanel: false,
      countries: [],
      regions: [],
      cities: [],

      currentId:0
    };
  }

  componentDidMount() {


    this.loadCountries();

    var itemId = this.props.id;
    if (itemId == null || itemId == 0) {
      //this.setState({
      //  id: 7,
      //  fullName: '',
      //  dateBirth: '',
      //  hasAuto: false,
      //  height: '',
      //  weight: '',
      //  idCountry: '',
      //  workDayStart: ''
      //});
      return;
    }

    var url = '/Address/GetOneById?id=' + itemId;
    userApiClient(url)
      .then(json => {
        this.setState({
          id: json.id,
          addressText: json.addressText,
          idCountry: json.idCountry,
          idPerson: json.idPerson,
          idRegion: json.idRegion,
          idCity: json.idCity,
        });
        this.onIdCountryChanged(this, this.state.idCountry);
        this.onIdRegionChanged(this, this.state.idRegion);

        var event = { target: { name: 'id', value: json.id, } };
        this.props.onChange(event);

        event = {target: {name: 'addressText', value: json.addressText,}};
        this.props.onChange(event);

        event = {target: {name: 'idCity', value: json.idCity,}};
        this.props.onChange(event);

        event = { target: { name: 'idPerson', value: json.idPerson,}};
        this.props.onChange(event);
      }
    )
    ;
  }

  loadCountries() {
    var url = '/Country/GetAll';
    userApiClient(url)
      .then(json => this.setState({ countries: json }));
  }
  
  loadRegions(id) {
    var url = '/Region/GetByCountryId?id=' + id;
    userApiClient(url)
      .then(json => this.setState({ regions: json }));
  }
  
  loadCities(id) {
    var url = '/City/GetByRegionId?id=' + id;
    userApiClient(url)
      .then(json => this.setState({ cities: json }));
  }

  reloadCountries() {
    this.loadCountries();
    this.setState({ openidCountryPanel: false });
  }


  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
    this.props.onChange(event);
  }

  btnAddCountryClick(t) {
    t.setState({ currentId: 0, openidCountryPanel: true });
  }

  btnEditCountryClick(t, id) {
    t.setState({ openidCountryPanel: true, currentId: id });
  }
  
  onIdCountryChanged(t, id) {
    t.setState({ idCountry: id }, this.loadRegions(id));
  }
  
  onIdRegionChanged(t, id) {
    t.setState({ idRegion: id }, this.loadCities(id));
  }


  render() {
    return (
      <Page title="Адрес">
        <Container maxWidth="lg">
          <Grid container spacing={3}>

            <form autoComplete="off" noValidate>
              <Card>
                { /*<CardHeader title="Адрес" />*/}
                <Divider />
                <CardContent>
                  <Grid container spacing={3}>

                    <Grid
                      item
                      md={12}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Адрес"
                        name="addressText"
                        onChange={this.handleChange}
                        value={this.state.addressText}
                        variant="outlined"
                      />
                    </Grid>


                    <Grid item md={12} xs={12}>
                      <BaseLookup
                        id="idCountry"
                        label="Страна"
                        value={this.state.idCountry}
                        onChange={this.handleChange}
                        data={this.state.countries}
                        name="idCountry" variant="outlined"
                        onChange={(e) => { this.onIdCountryChanged(this, e.target.value) }}
                        onButtonAddClick={() => this.btnAddCountryClick(this)}
                        onButtonEditClick={(id) => this.btnEditCountryClick(this, id)} >

                        
                        <Dialog open={this.state.openidCountryPanel} onClose={() => this.handleClose()} aria-labelledby="form-dialog-title">
                          <DialogTitle id="form-dialog-title">Страна</DialogTitle>
                          <DialogContent>
                          <CountryAddEditView

                              onBtnCancelClick={() => {
                                this.setState({ openidCountryPanel: false });
                              }}

                              onBtnOkClick={(id) => {
                                this.reloadCountries();
                                //alert(id);
                                this.onIdCountryChanged(this, id);
                              }}

                              openPanel={this.state.openidCountryPanel}
                              id={this.state.currentId}
                              isPopup={ true}
                          />

                          </DialogContent>
                        </Dialog>
                        
                      </BaseLookup>
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <BaseLookup
                        id="idRegion"
                        label="Область"
                        value={this.state.idRegion}
                        onChange={this.handleChange}
                        data={this.state.regions}
                        name="idRegion" variant="outlined"
                        onChange={(e) => { this.onIdRegionChanged(this, e.target.value) }}
                        onButtonAddClick={() => this.btnAddCountryClick(this)}
                        onButtonEditClick={(id) => this.btnEditCountryClick(this, id)} >
                        { /*
                        <SlidingPanel type='right' isOpen={this.state.openidCountryPanel} size='30' panelClassName="additional-class">

                          <CountryAddEditView

                            onBtnCancelClick={() => {
                              this.setState({ openidCountryPanel: false });
                            }}

                            onBtnOkClick={(id) => {
                              this.reloadCountries();
                            }}

                            openPanel={this.state.openidCountryPanel}
                            id={this.state.currentId}
                          />

                        </SlidingPanel>
                        */}
                      </BaseLookup>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <BaseLookup
                        id="idCity"
                        label="Город"
                        value={this.state.idCity}
                        onChange={this.handleChange}
                        data={this.state.cities}
                        name="idCity" variant="outlined"
                        onButtonAddClick={() => this.btnAddCountryClick(this)}
                        onButtonEditClick={(id) => this.btnEditCountryClick(this, id)} >
                        { /*
                        <SlidingPanel type='right' isOpen={this.state.openidCountryPanel} size='30' panelClassName="additional-class">

                          <CountryAddEditView

                            onBtnCancelClick={() => {
                              this.setState({ openidCountryPanel: false });
                            }}

                            onBtnOkClick={(id) => {
                              this.reloadCountries();
                            }}

                            openPanel={this.state.openidCountryPanel}
                            id={this.state.currentId}
                          />

                        </SlidingPanel>
                        */}
                      </BaseLookup>
                    </Grid>


                  </Grid>
                </CardContent>

              </Card>
              <Divider />
              { /*
              <Box display="flex" justifyContent="flex-end" p={2}>

                <Box m={2}>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => {
                      var data = {
                        id: this.state.id,

                        idCity: this.state.idCity - 0, // -0 для интовых полей, чтобы конвертировать в инт
                        addressText: this.state.addressText,
                        idPerson: this.state.idPerson,
                      };

                      fetch('/Address/AddOrUpdate', {
                        method: "POST",
                        body: JSON.stringify(data),
                        headers: { "Content-type": "application/json; charset=UTF-8" }
                      })
                        .then(response => response.json())
                        .then(json => {
                          var id = this.state.id;
                          this.props.onBtnOkClick(id);
                          console.log(json)
                        })
                        .catch(err => console.log(err));
                    }}
                  >
                    Сохранить
                  </Button>
                </Box>

                <Box m={2}>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => this.props.onBtnCancelClick()}
                  >
                    Отмена
                  </Button>
                </Box>

              </Box>
              */}
            </form>
          </Grid>
        </Container>
      </Page>
    );
  }
};
