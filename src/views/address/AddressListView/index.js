import React, { useState } from 'react';
import { userApiClient } from 'src/components/ApiHelper'
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import AddressAddEditView from 'src/views/address/AddressAddEditView';
import PopupGrid from 'src/components/PopupGrid';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


export default class AddressListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      openPanel: false,
      currentId: 0,
      personId: 0, 

      id: 0,

      idCity: 0,
      addressText: '',
    };
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleClose = () => {
    //setOpen(false);
  };

  onButtonAddEditClick(t, id) {
    t.setState({ currentId: id, openPanel: true });
  }


  loadAddresss() {
    var url = '/Address/GetByPersonId?id=' + this.props.personId;
    userApiClient(url)
      .then(json => this.setState({ data: json }));
  }

  componentDidUpdate() {
    if (this.props.personId == 0) return;
    if (this.props.personId == this.state.personId) return;
    this.setState({ personId: this.props.personId });
    this.loadAddresss();
  }

  componentDidMount() {
  }

  render() {
    const columns = [
      {
        name: "addressText",
        label: "Адрес",
        options: {
          filter: true,
        }
      },
      {
        name: 'cityName',
        label: "Город",
        options: {
          filter: true,
        }
      },
      {
        name: 'regionName',
        label: "Регион",
        options: {
          filter: true,
        }
      },
      {
        name: 'countryName',
        label: "Страна",
        options: {
          filter: true,
        }
      },
    ];
    return (
      <Page title="Адреса">

        <Container maxWidth={false}>

          <PopupGrid
            title="Адреса 1"
            subtitle="Адреса 2"
            onButtonAddEditClick={(id) => this.onButtonAddEditClick(this, id)}
            onItemDeleted={() => this.loadAddresss(this)}
            columns={columns}
            data={this.state.data}
            controllerName="Address" />

          <Dialog open={this.state.openPanel} onClose={() => this.handleClose()} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Адрес</DialogTitle>
            <DialogContent>
              <AddressAddEditView
                onChange={(event) => this.handleChange(event)}
                onItemDeleted={() => {
                  this.loadAddresss();
                }}
                openPanel={this.state.openPanel}
                id={this.state.currentId}
                personId={this.props.personId}
              />
            </DialogContent>
            <DialogActions>
              <Button
                color="secondary"
                variant="contained"
                onClick={() => {
                  var data = {
                    id: this.state.id,

                    idCity: this.state.idCity - 0, // -0 для интовых полей, чтобы конвертировать в инт
                    addressText: this.state.addressText,
                    idPerson: this.props.personId,
                  };

                  //alert(JSON.stringify(data));

                  fetch('/Address/AddOrUpdate', {
                    method: "POST",
                    body: JSON.stringify(data),
                    headers: { "Content-type": "application/json; charset=UTF-8" }
                  })
                    .then(response => response.json())
                    .then(json => {
                      this.setState({ openPanel: false });

                      this.loadAddresss();
                    })
                    .catch(err => console.log(err));
                }}
              >
                Сохранить
              </Button>
              <Button
                color="primary"
                variant="contained"
                onClick={() => this.setState({ openPanel: false })}
              >
                Отмена
                  </Button>
            </DialogActions>
          </Dialog>


 


        </Container>
      </Page>
    );
  }

};


